// Initialize a new PIXI Application
const app = new PIXI.Application();
// Add the application view to the document body
document.body.appendChild(app.view);

// Prepare circle texture, which will be used as a brush
const brush = new PIXI.Graphics().beginFill(0xffffff).drawCircle(0, 0, 100);

// Create a line that will interpolate the drawn points
const line = new PIXI.Graphics();

// Add textures to PIXI assets
PIXI.Assets.add("t1", "scratch-bg.png");
PIXI.Assets.add("t2", "adlicious-bg.png");
// Load the textures and call the setup function when done
PIXI.Assets.load(["t1", "t2"]).then(setup);

function setup() {
    // Get the screen dimensions
    const { width, height } = app.screen;
    // Define the stage size
    const stageSize = { width, height };

    // Create background and imageToReveal sprites with appropriate dimensions
    const background = Object.assign(PIXI.Sprite.from("t1"), stageSize);
    const imageToReveal = Object.assign(PIXI.Sprite.from("t2"), stageSize);
    // Create a render texture with the stage size
    const renderTexture = PIXI.RenderTexture.create(stageSize);
    // Create a sprite from the render texture
    const renderTextureSprite = new PIXI.Sprite(renderTexture);

    // Set the mask for the image to reveal
    imageToReveal.mask = renderTextureSprite;

    // Add sprites to the stage
    app.stage.addChild(background, imageToReveal, renderTextureSprite);

    // Make the stage interactive and define the hit area
    app.stage.interactive = true;
    app.stage.hitArea = app.screen;

    // Attach event listeners to the stage
    app.stage
        .on("pointerdown", pointerDown)
        .on("pointerup", pointerUp)
        .on("pointerupoutside", pointerUp)
        .on("pointermove", pointerMove);

    // Initialize variables for dragging and the last drawn point
    let dragging = false;
    let lastDrawnPoint = null;

    // Define the pointerMove function
    function pointerMove({ global: { x, y } }) {
        if (dragging) {
            // Set the brush position
            brush.position.set(x, y);
            // Render the brush onto the render texture
            app.renderer.render(brush, {
                renderTexture,
                clear: false,
                skipUpdateTransform: false,
            });
            // Connect the previous drawn point to the current one using a line
            if (lastDrawnPoint) {
                line.clear()
                    .lineStyle({ width: 100, color: 0xffffff })
                    .moveTo(lastDrawnPoint.x, lastDrawnPoint.y)
                    .lineTo(x, y);
                // Render the line onto the render texture
                app.renderer.render(line, {
                    renderTexture,
                    clear: false,
                    skipUpdateTransform: false,
                });
            }
            // Update the last drawn point
            lastDrawnPoint = lastDrawnPoint || new PIXI.Point();
            lastDrawnPoint.set(x, y);
        }
    }

    // Define the pointerDown function
    function pointerDown(event) {
        dragging = true;
        pointerMove(event);
    }

    // Define the pointerUp function
    function pointerUp(event) {
        dragging = false;
        lastDrawnPoint = null;
    }
}
