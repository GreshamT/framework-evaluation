window.addEventListener("resize", () => {
    window.location.reload();
});

const gameScene = {
    ball: null,
    leftPaddle: null,
    rightPaddle: null,

    preload: preload,
    create: create,
    update: update,
};

const config = {
    type: Phaser.AUTO,
    width: 600,
    height: 400,
    backgroundColor: "#000000",
    physics: {
        default: "arcade",
        arcade: {
            gravity: { y: 0, x: 0 },
            debug: false,
        },
    },
    scene: gameScene,
};

const game = new Phaser.Game(config);

function preload() {}

function create() {
    // Create paddles
    this.leftPaddle = this.add.rectangle(
        10,
        this.cameras.main.height / 2,
        10,
        60,
        0xffffff
    );
    this.rightPaddle = this.add.rectangle(
        this.cameras.main.width - 10,
        this.cameras.main.height / 2,
        10,
        60,
        0xffffff
    );

    // Create ball
    this.ball = this.add.ellipse(
        this.cameras.main.width / 2,
        this.cameras.main.height / 2,
        12,
        12,
        0xffffff
    );

    // Enable physics
    this.physics.add.existing(this.leftPaddle, false);
    this.physics.add.existing(this.rightPaddle, false);
    this.physics.add.existing(this.ball, false);

    // Set the balls velocity
    this.ball.body.setVelocity(150, 150);

    // Make paddles immovable and enable collisions
    this.leftPaddle.body.immovable = true;
    this.rightPaddle.body.immovable = true;
    this.leftPaddle.body.setCollideWorldBounds(true);
    this.rightPaddle.body.setCollideWorldBounds(true);

    // Enable collisions with world bounds
    this.leftPaddle.body.setCollideWorldBounds(true);
    this.rightPaddle.body.setCollideWorldBounds(true);
}

function update() {
    // Handle input in the update function for smoother movement
    const cursorKeys = this.input.keyboard.createCursorKeys();
    const customKeys = {
        W: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
        S: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
    };
    const paddleSpeed = 5;

    if (customKeys.W.isDown) {
        this.leftPaddle.y -= paddleSpeed;
    }
    if (customKeys.S.isDown) {
        this.leftPaddle.y += paddleSpeed;
    }
    if (cursorKeys.up.isDown) {
        this.rightPaddle.y -= paddleSpeed;
    }
    if (cursorKeys.down.isDown) {
        this.rightPaddle.y += paddleSpeed;
    }

    //  Physics collisions
    this.physics.world.collide(this.ball, this.leftPaddle, () =>
        this.ball.body.setVelocityX(150)
    );
    this.physics.world.collide(this.ball, this.rightPaddle, () =>
        this.ball.body.setVelocityX(-150)
    );

    // Add a small buffer to prevent the ball from getting stuck along the edges
    const buffer = 1;
    if (
        this.ball.y - this.ball.height / 2 <= buffer ||
        this.ball.y + this.ball.height / 2 >= this.cameras.main.height - buffer
    ) {
        this.ball.body.setVelocityY(-this.ball.body.velocity.y);
    }

    if (this.ball.y <= 0 || this.ball.y >= this.cameras.main.height) {
        this.ball.body.setVelocityY(-this.ball.body.velocity.y);
    }

    if (this.ball.x <= 0 || this.ball.x >= this.cameras.main.width) {
        // Update scores and reset ball position
        this.ball.setPosition(
            this.cameras.main.width / 2,
            this.cameras.main.height / 2
        );
        this.ball.body.setVelocity(150, 150);
    }
}
