// Add an event listener for window resize events
window.addEventListener("resize", () => {
    // Reload the window when resized
    window.location.reload();
});

// Define the game scene object, containing the ball and paddles
const gameScene = {
    ball: null,
    leftPaddle: null,
    rightPaddle: null,

    // Functions for creating and updating the game objects
    create: create,
    update: update,
};

// Configuration for the Phaser game engine
const config = {
    type: Phaser.AUTO, // Automatically choose renderer (WebGL or Canvas)
    width: 600,
    height: 400,
    backgroundColor: "#000000",
    physics: {
        default: "arcade",
        arcade: {
            gravity: { y: 0, x: 0 }, // No gravity in this game
            debug: false, // Disable debug mode
        },
    },
    scene: gameScene, // Set the game scene
};

// Create a new Phaser game with the given configuration
const game = new Phaser.Game(config);

// Create function for creating game objects
function create() {
    // Create the ball object
    this.ball = this.add.circle(
        config.width / 2,
        config.height / 2,
        10,
        0xff0000
    );
    // Add physics to the ball
    this.physics.add.existing(this.ball);
    // Set the initial velocity of the ball
    this.ball.body.setVelocity(200);
    // Make the ball collide with the world bounds
    this.ball.body.setCollideWorldBounds(true, 1, 1);

    // Create left and right paddles
    this.leftPaddle = this.add.rectangle(
        20,
        this.cameras.main.height / 2,
        10,
        80,
        0x808080
    );
    this.rightPaddle = this.add.rectangle(
        this.cameras.main.width - 20,
        this.cameras.main.height / 2,
        10,
        80,
        0x808080
    );

    // Add physics to the paddles
    this.physics.add.existing(this.leftPaddle, false);
    this.physics.add.existing(this.rightPaddle, false);

    // Make paddles immovable and enable collisions
    this.leftPaddle.body.immovable = true;
    this.rightPaddle.body.immovable = true;
    this.leftPaddle.body.setCollideWorldBounds(true);
    this.rightPaddle.body.setCollideWorldBounds(true);
}

// Update function for handling game logic
function update() {
    // Create custom input keys for paddle movement
    const cursorKeys = this.input.keyboard.createCursorKeys();
    const customKeys = {
        W: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
        S: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
    };
    const paddleSpeed = 5;

    // Move the paddles based on input keys
    if (customKeys.W.isDown) {
        this.leftPaddle.y -= paddleSpeed;
    }
    if (customKeys.S.isDown) {
        this.leftPaddle.y += paddleSpeed;
    }
    if (cursorKeys.up.isDown) {
        this.rightPaddle.y -= paddleSpeed;
    }
    if (cursorKeys.down.isDown) {
        this.rightPaddle.y += paddleSpeed;
    }

    // Handle physics collisions between the ball and paddles
    this.physics.world.collide(this.ball, this.leftPaddle, () =>
        this.ball.body.setVelocityX(300)
    );
    this.physics.world.collide(this.ball, this.rightPaddle, () =>
        this.ball.body.setVelocityX(-500)
    );

}
