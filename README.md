# Framework-evaluation

Task: Provide a deeper evaluation and build a very basic demo using the frameworks ThreeJs, Phaser, PixiJs, and GreenSock

## Hosted Links

- ThreeJs - https://symphonious-bavarois-e82b6c.netlify.app/
- Phaser - https://grand-conkies-f246fd.netlify.app/ 
- PixiJs - https://clever-sawine-83fe33.netlify.app/